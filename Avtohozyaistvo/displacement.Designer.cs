﻿namespace Avtohozyaistvo
{
    partial class displacement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.position_tb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.date_tb = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.delete_btn = new System.Windows.Forms.Button();
            this.add_btn = new System.Windows.Forms.Button();
            this.table_GV = new System.Windows.Forms.DataGridView();
            this.associate_tb = new System.Windows.Forms.ComboBox();
            this.couse_tb = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.displacement_gb = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.table_GV)).BeginInit();
            this.displacement_gb.SuspendLayout();
            this.SuspendLayout();
            // 
            // position_tb
            // 
            this.position_tb.FormattingEnabled = true;
            this.position_tb.Location = new System.Drawing.Point(116, 45);
            this.position_tb.Name = "position_tb";
            this.position_tb.Size = new System.Drawing.Size(358, 21);
            this.position_tb.TabIndex = 41;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(45, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 40;
            this.label6.Text = "Должность";
            // 
            // date_tb
            // 
            this.date_tb.Location = new System.Drawing.Point(116, 72);
            this.date_tb.Name = "date_tb";
            this.date_tb.Size = new System.Drawing.Size(358, 20);
            this.date_tb.TabIndex = 37;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 36;
            this.label4.Text = "Дата приказа";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Сотрудник";
            // 
            // delete_btn
            // 
            this.delete_btn.Location = new System.Drawing.Point(695, 340);
            this.delete_btn.Name = "delete_btn";
            this.delete_btn.Size = new System.Drawing.Size(97, 23);
            this.delete_btn.TabIndex = 29;
            this.delete_btn.Text = "Удалить";
            this.delete_btn.UseVisualStyleBackColor = true;
            this.delete_btn.Click += new System.EventHandler(this.delete_btn_Click);
            // 
            // add_btn
            // 
            this.add_btn.Location = new System.Drawing.Point(12, 341);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(97, 23);
            this.add_btn.TabIndex = 28;
            this.add_btn.Text = "Добавить в БД";
            this.add_btn.UseVisualStyleBackColor = true;
            this.add_btn.Click += new System.EventHandler(this.add_btn_Click);
            // 
            // table_GV
            // 
            this.table_GV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.table_GV.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.table_GV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_GV.Location = new System.Drawing.Point(12, 12);
            this.table_GV.Name = "table_GV";
            this.table_GV.Size = new System.Drawing.Size(780, 111);
            this.table_GV.TabIndex = 27;
            // 
            // associate_tb
            // 
            this.associate_tb.FormattingEnabled = true;
            this.associate_tb.Location = new System.Drawing.Point(116, 19);
            this.associate_tb.Name = "associate_tb";
            this.associate_tb.Size = new System.Drawing.Size(358, 21);
            this.associate_tb.TabIndex = 42;
            // 
            // couse_tb
            // 
            this.couse_tb.Location = new System.Drawing.Point(116, 98);
            this.couse_tb.Name = "couse_tb";
            this.couse_tb.Size = new System.Drawing.Size(358, 96);
            this.couse_tb.TabIndex = 43;
            this.couse_tb.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 44;
            this.label2.Text = "Причина перевода";
            // 
            // displacement_gb
            // 
            this.displacement_gb.Controls.Add(this.associate_tb);
            this.displacement_gb.Controls.Add(this.label2);
            this.displacement_gb.Controls.Add(this.label1);
            this.displacement_gb.Controls.Add(this.couse_tb);
            this.displacement_gb.Controls.Add(this.label4);
            this.displacement_gb.Controls.Add(this.date_tb);
            this.displacement_gb.Controls.Add(this.position_tb);
            this.displacement_gb.Controls.Add(this.label6);
            this.displacement_gb.Location = new System.Drawing.Point(162, 129);
            this.displacement_gb.Name = "displacement_gb";
            this.displacement_gb.Size = new System.Drawing.Size(480, 206);
            this.displacement_gb.TabIndex = 45;
            this.displacement_gb.TabStop = false;
            this.displacement_gb.Text = "Сведения о перемещении";
            // 
            // displacement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 375);
            this.Controls.Add(this.displacement_gb);
            this.Controls.Add(this.delete_btn);
            this.Controls.Add(this.add_btn);
            this.Controls.Add(this.table_GV);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "displacement";
            this.Text = "Сведения о перемещении";
            this.Load += new System.EventHandler(this.displacement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.table_GV)).EndInit();
            this.displacement_gb.ResumeLayout(false);
            this.displacement_gb.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox position_tb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker date_tb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button delete_btn;
        private System.Windows.Forms.Button add_btn;
        private System.Windows.Forms.DataGridView table_GV;
        private System.Windows.Forms.ComboBox associate_tb;
        private System.Windows.Forms.RichTextBox couse_tb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox displacement_gb;
    }
}