﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Collections;

namespace Avtohozyaistvo
{
    public partial class employees : Form
    {
        MySqlConnection connection;
        ShowTables show;
        CheckEnter checkEnter = new CheckEnter();
        bool surname_ch = false;
        bool name_ch = false;
        bool patronymic_ch = false;
        bool address_ch = false;
        bool position_ch = false;
        string ability = null;
        string show_request = "SELECT em.surname AS Фамилия, em.name AS Имя, em.patronymic AS Отчество, " +
            "em.address AS Адрес, em.date_birth AS Дата_рождения, pos.position AS Должность " +
            "FROM employees em " +
            "INNER JOIN positions pos ON pos.id_position = em.id_position";
        public employees(MySqlConnection connection, string ability)
        {
            string request = null;

            InitializeComponent();
            this.connection = connection;
            this.ability = ability;
            table_GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            table_GV.AllowUserToAddRows = false;
            table_GV.MultiSelect = false;
            table_GV.ReadOnly = true;
            if(ability == "read")
            {
                add_btn.Visible = false;
                delete_btn.Visible = false;
                employees_gb.Visible = false;
                table_GV.Height = this.Height - 60;
            }
            connection.Open();
            show = new ShowTables(connection, table_GV, "employees");
            show.take_data(show_request);

            request = "SELECT position FROM positions";
            show.get_selector(request, position_tb);
            connection.Close();

            surname_tb.TextChanged += Surname_tb_TextChanged;
            name_tb.TextChanged += Name_tb_TextChanged;
            patronymic_tb.TextChanged += Patronymic_tb_TextChanged;
            address_tb.TextChanged += Address_tb_TextChanged;
            position_tb.TextChanged += Position_tb_TextChanged;
        }

        private void Position_tb_TextChanged(object sender, EventArgs e)
        {
            position_ch = checkEnter.checkFormatComboBox(null, position_tb.Text, position_tb, this.employees_gb);
        }

        private void Address_tb_TextChanged(object sender, EventArgs e)
        {
            address_ch = checkEnter.checkFormatTextBox(@"^[а-яА-Я \d.,-]+$", address_tb.Text, address_tb, this.employees_gb);
        }

        private void Patronymic_tb_TextChanged(object sender, EventArgs e)
        {
            patronymic_ch = checkEnter.checkFormatTextBox(@"^[а-яА-Я]+$", patronymic_tb.Text, patronymic_tb, this.employees_gb);
        }

        private void Name_tb_TextChanged(object sender, EventArgs e)
        {
            name_ch = checkEnter.checkFormatTextBox(@"^[а-яА-Я]+$", name_tb.Text, name_tb, this.employees_gb);
        }

        private void Surname_tb_TextChanged(object sender, EventArgs e)
        {
            surname_ch = checkEnter.checkFormatTextBox(@"^[а-яА-Я]+$", surname_tb.Text, surname_tb, this.employees_gb);
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            if (surname_ch && name_ch && patronymic_ch && address_ch && position_ch)
            {
                string request = null;

                string surname = surname_tb.Text;
                string name = name_tb.Text;
                string patronymic = patronymic_tb.Text;
                string address = address_tb.Text;
                DateTime date_birth = date_tb.Value;
                string position = position_tb.Text;

                int id_position = 0;

                request = "SELECT id_position FROM positions WHERE position =\"" + position + "\"";
                connection.Open();
                id_position = show.get_id(request);
                connection.Close();

                request = "INSERT INTO employees(surname, name, patronymic, address, date_birth, id_position) " +
                    "VALUES(\"" + surname + "\",\"" + name + "\",\"" + patronymic + "\"," +
                    "\"" + address + "\",\"" + date_birth.Date.ToString("yyyy-MM-dd") + "\"," + id_position + ");";
                connection.Open();
                show.make_request(request);
                connection.Close();
            }
            else
            {
                MessageBox.Show("Введите все данные");
            }
        }

        private void delete_btn_Click(object sender, EventArgs e)
        {
            string request = null;

            DataGridViewRow row = table_GV.SelectedRows[0];
            string surname = (string)row.Cells[0].Value;
            string name = (string)row.Cells[1].Value;
            string patronymic = (string)row.Cells[2].Value;

            request = "DELETE FROM employees " +
                "WHERE surname=\"" + surname + "\" " +
                "AND name=\"" + name +"\"" +
                "AND patronymic=\"" + patronymic + "\";";
            connection.Open();
            show.make_request(request);
            connection.Close();
        }

        private void employees_Load(object sender, EventArgs e)
        {
            connection.Open();
            show.update_database();
            connection.Close();
        }
    }
}
