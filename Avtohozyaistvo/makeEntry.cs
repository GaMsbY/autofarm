﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Avtohozyaistvo
{
    public partial class makeEntry : Form
    {
        MySqlConnection connection;
        CheckEnter checkEnter = new CheckEnter();
        ShowTables show;
        bool surname_ch = false;
        bool address_ch = false;
        bool telephon_ch = false;
        bool start_address_ch = false;
        bool end_address_ch = false;
        bool weight_ch = false;
        bool cargo_ch = false;
        int _height = 25;
        int _width = 100;
        Button[] menu;
        int menu_size = 11;
        string access = null;
        string ability = null;
        string[] menu_name = new string[11] {"Сотрудники","Перемещения","Должности", "Клиенты", "Автомобили", "Состояния", "Классности", "Грузы", "Водители", "Заказы", "Связи" };
        public makeEntry()
        {
            InitializeComponent();
            try
            {
                connection = DBUtils.GetDBConnection();
            }
            catch(MySqlException ex)
            {
                MessageBox.Show("Ошибка");
            }

            time_tb.Format = DateTimePickerFormat.Time;
            time_tb.ShowUpDown = true;
            this.Resize += new System.EventHandler(this.MForm_Resize);
            this.MinimumSize = new Size(415, 325);

            show = new ShowTables(connection, "makeentry");
            
            string request = "SELECT cargo FROM cargos";
            try
            {
                connection.Open();
                show.get_selector(request, cargo_tb);
                connection.Close();
            }
            catch(Exception error)
            {
                MessageBox.Show("База  данных не активна\nПроверьте состояние базы данных и попробуйте еще раз.");
            }

            surname_tb.TextChanged += Surname_tb_TextChanged;
            address_tb.TextChanged += Address_tb_TextChanged;
            telephon_tb.TextChanged += Telephon_tb_TextChanged;
            start_address_tb.TextChanged += Start_address_tb_TextChanged;
            end_address_tb.TextChanged += End_address_tb_TextChanged;
            weight_tb.TextChanged += Weight_tb_TextChanged;
            cargo_tb.TextChanged += Cargo_tb_TextChanged;
        }

        private void Cargo_tb_TextChanged(object sender, EventArgs e)
        {
            cargo_ch = checkEnter.checkFormatComboBox(null, cargo_tb.Text, cargo_tb, this.makeentry_gb);
        }

        private void Weight_tb_TextChanged(object sender, EventArgs e)
        {
            weight_ch = checkEnter.checkFormatTextBox(@"^\d+$", weight_tb.Text, weight_tb, this.makeentry_gb);
        }

        private void End_address_tb_TextChanged(object sender, EventArgs e)
        {
            end_address_ch = checkEnter.checkFormatTextBox(@"^[а-яА-Я\d ,.]+$", end_address_tb.Text, end_address_tb, this.makeentry_gb);
        }

        private void Start_address_tb_TextChanged(object sender, EventArgs e)
        {
            start_address_ch = checkEnter.checkFormatTextBox(@"^[а-яА-Я\d ,.]+$", start_address_tb.Text, start_address_tb, this.makeentry_gb);
        }

        private void Telephon_tb_TextChanged(object sender, EventArgs e)
        {
            telephon_ch = checkEnter.checkFormatTextBox(@"^[-\d]+$", telephon_tb.Text, telephon_tb, this.makeentry_gb);
        }

        private void Address_tb_TextChanged(object sender, EventArgs e)
        {
            address_ch = checkEnter.checkFormatTextBox(@"^[а-яА-Я\d ,.]+$", address_tb.Text, address_tb, this.makeentry_gb);
        }

        private void Surname_tb_TextChanged(object sender, EventArgs e)
        {
            surname_ch = checkEnter.checkFormatTextBox(@"^[а-яА-Я]+$", surname_tb.Text, surname_tb, this.makeentry_gb);
        }

        private void menu_button_Click(object sender, EventArgs e)
        {
            try
            {
                switch ((sender as Button).Text)
                {
                    case "Сотрудники":
                        {
                            employees empl = new employees(connection, ability);
                            empl.Show();
                            break;
                        }
                    case "Перемещения":
                        {
                            displacement dis = new displacement(connection, ability);
                            dis.Show();
                            break;
                        }
                    case "Должности":
                        {
                            positions pos = new positions(connection, ability);
                            pos.Show();
                            break;
                        }
                    case "Клиенты":
                        {
                            clients cl = new clients(connection, ability);
                            cl.Show();
                            break;
                        }
                    case "Автомобили":
                        {
                            autos au = new autos(connection, ability);
                            au.Show();
                            break;
                        }
                    case "Состояния":
                        {
                            States st = new States(connection, ability);
                            st.Show();
                            break;
                        }
                    case "Классности":
                        {
                            classes classe = new classes(connection, ability);
                            classe.Show();
                            break;
                        }
                    case "Грузы":
                        {
                            cargos carg = new cargos(connection, ability);
                            carg.Show();
                            break;
                        }
                    case "Водители":
                        {
                            drivers dr = new drivers(connection, ability);
                            dr.Show();
                            break;
                        }
                    case "Заказы":
                        {
                            entrys en = new entrys(connection, ability);
                            en.Show();
                            break;
                        }
                    case "Связи":
                        {
                            driver_auto da = new driver_auto(connection, ability);
                            da.Show();
                            break;
                        }
                }
            }
            catch(Exception myError)
            {
                MessageBox.Show("Упс!!! Что-то пошло не так,\n попытайтесь еще раз!");
            }
        }

        private void MForm_Resize(object sender, EventArgs e)
        {
            if (menu != null)
            {
                int _x = 0;
                int _y = (this.Height - _height * menu_size) / 2 - 20;
                for (int i = 0; i < menu.Length; i++)
                {
                    menu[i].Location = new System.Drawing.Point(_x, _y);
                    _y += _height;
                }
            }
        }

        private void make_entry_btn_Click(object sender, EventArgs e)
        {
            int id_driver_add = 0;
            int id_auto_add = 0;
            string auto = null;
            string driver = null;
            string request = null;
            List<int> busyCar = new List<int>();
            List<int> busyDriver = new List<int>();

            telephon_ch = checkEnter.checkFormatTextBox(@"^8-\d{3}-\d{3}-\d{2}-\d{2}$", telephon_tb.Text, telephon_tb, this.makeentry_gb);

            if(surname_ch && address_ch && telephon_ch && start_address_ch && end_address_ch && weight_ch && cargo_ch)
            {
                bool accept = false;

                //Получаем id водителей и автомобилей занятых в уазанное время
                DateTime dateTime = date_tb.Value.Date + time_tb.Value.TimeOfDay;
                DateTime end_dateTime = dateTime.AddHours(-1);
                string date_time_work = dateTime.ToString("yyyy-MM-dd HH:mm:ss");
                string date_time_end = end_dateTime.ToString("yyyy-MM-dd HH:mm:ss");
                request = "SELECT id_auto, id_driver " +
                    "FROM entrys " +
                    "WHERE date_work >= \"" + date_time_end + "\" " +
                    "AND date_work <=\"" + date_time_work + "\"";
                connection.Open();
                MySqlCommand command = new MySqlCommand(request, connection);
                MySqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        busyCar.Add((int)reader.GetValue(0));
                        busyDriver.Add((int)reader.GetValue(1));
                    }
                }
                connection.Close();
                //-----------------------------------------------------------------------------

                //Получаем id груза заказа
                request = "SELECT id_cargo FROM cargos WHERE cargo= \"" + cargo_tb.Text + "\"";
                connection.Open();
                int id_cargo = show.get_id(request);
                connection.Close();
                //-----------------------------------------------------------------------------

                
                //Получаем id водителя и машины способных взять этот заказ
                foreach (int id_driver in busyDriver)
                {
                    driver += " AND dr.id_driver<>" + id_driver;
                }
                foreach (int id_auto in busyCar)
                {
                    auto += " AND au.id_auto<>" + id_auto;
                }
                request = "SELECT da.id_driver, da.id_auto FROM driver_auto da " +
                    "INNER JOIN autos au ON au.id_auto = da.id_auto " +
                    "INNER JOIN drivers dr ON dr.id_driver = da.id_driver " +
                    "WHERE au.id_cargo =" + id_cargo + " AND au.id_state = 2" + auto + driver + " " +
                    "AND au.carrying_capacity >=" + Convert.ToInt32(weight_tb.Text);
                connection.Open();
                command = new MySqlCommand(request, connection);
                reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        id_driver_add = (int)reader.GetValue(0);
                        id_auto_add = (int)reader.GetValue(1);
                        break;
                    }
                    accept = true;
                }
                else
                {
                    accept = false;
                    MessageBox.Show("В заказе отказано, отсутствуют машины для выполнения данного заказа");
                }
                connection.Close();
                //-----------------------------------------------------------------------------

                //Оформляем заказ
                if (accept)
                {
                    request = "SELECT unit_cost FROM cargos WHERE cargo= \"" + cargo_tb.Text + "\"";
                    connection.Open();
                    int unit_cost = show.get_id(request);
                    connection.Close();
                    DateTime date_order = DateTime.Now;
                    exitDocument exDoc = new exitDocument(connection, surname_tb.Text, address_tb.Text, telephon_tb.Text, id_auto_add, id_driver_add, date_order, id_cargo, start_address_tb.Text, end_address_tb.Text, dateTime, Convert.ToInt32(weight_tb.Text),  unit_cost);
                    DialogResult dialogResult = exDoc.ShowDialog();
                    if (dialogResult == DialogResult.OK)
                    {
                        request = "INSERT INTO clients(surname, address, telephon) " +
                            "VALUES(\"" + surname_tb.Text + "\",\"" + address_tb.Text + "\",\"" + telephon_tb.Text + "\");";
                        connection.Open();
                        show.make_request_unupdate(request);
                        connection.Close();

                        request = "SELECT id_client FROM clients " +
                            "WHERE surname =\"" + surname_tb.Text + "\" AND telephon=\"" + telephon_tb.Text + "\"";
                        connection.Open();
                        int id_client = show.get_id(request);
                        connection.Close();

                        //сохранение заказа в БД
                        request = "INSERT INTO entrys(id_client, id_auto, id_driver, date_order, id_cargo, " +
                            "start_address, end_address, date_work, weight, state_e, cost_en) " +
                            "VALUES(" + id_client + "," + id_auto_add + "," + id_driver_add + "," +
                            "\"" + date_order.Date.ToString("yyyy-MM-dd") + "\"," + id_cargo + ",\"" + start_address_tb.Text + "\"," +
                            "\"" + end_address_tb.Text + "\",\"" + dateTime.ToString("yyyy-MM-dd HH:mm:ss") + "\"," +
                            "" + Convert.ToInt32(weight_tb.Text) + ",\"Принят\"," + Convert.ToInt32(weight_tb.Text) * unit_cost + ");";
                        connection.Open();
                        show.make_request_unupdate(request);
                        connection.Close();

                        //запрос выходного документа для сохранения  в ворд
                        
                        request = "SELECT  cli.surname, cli.address, cli.telephon, au.mark, au.number_g, dr.surname, dr.name," +
                          " dr.patronymic, en.date_order, carg.cargo, en.start_address, en.end_address," +
                          " en.date_work, en.weight, carg.unit_cost, en.cost_en, en.id_entry " +
                          "FROM ENTRYS EN INNER JOIN  CLIENTS CLI ON cli.id_client = en.id_client " +
                          "INNER JOIN CARGOS CARG ON en.id_cargo = carg.id_cargo " +
                          "INNER JOIN DRIVERS DR  ON en.id_driver = dr.id_driver " +
                          "INNER JOIN autos AU ON en.id_auto = au.id_auto " +
                          "WHERE cli.surname=\"" + surname_tb.Text + "\" " +
                          "AND cli.telephon=\"" + telephon_tb.Text + "\" " +
                          "AND cli.address=\""+ address_tb.Text + "\" " +
                          "AND en.date_order=\"" + date_order.Date.ToString("yyyy-MM-dd") +"\"";
                        connection.Open();
                        command = new MySqlCommand(request, connection);
                        reader = command.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                string surname = "Фамилия клиента: " + (string)reader.GetValue(0);
                                string address = "Адрес клиента: " + (string)reader.GetValue(1);
                                string telephon = "Телефон клиента: " + (string)reader.GetValue(2);
                                string execotor = "Исполнитель: " + (string)reader.GetValue(5) + " " + (string)reader.GetValue(6) + " " + (string)reader.GetValue(7);
                                string executor_auto = "Автомобиль: " + (string)reader.GetValue(3) + " " + (string)reader.GetValue(4);
                                string date = "Дата заказа: " + ((DateTime)reader.GetValue(8)).ToString();
                                string cargo = "Груз: " + (string)reader.GetValue(9) + " (" + ((int)reader.GetValue(14)).ToString() + "за 1ц.)" + "-" + ((int)reader.GetValue(13)).ToString() + "ц.";
                                string start = "Адрез подачи: " + (string)reader.GetValue(10);
                                string end = "Адрез назначения: " + (string)reader.GetValue(11);
                                string date_work = "Дата выполнения: " + ((DateTime)reader.GetValue(12)).ToString();
                                string cost = "Стоимость: " + ((int)reader.GetValue(15)).ToString();
                                int id_entry = (int)reader.GetValue(16);
                                wordWork word = new wordWork(id_entry, surname, address, telephon, execotor, executor_auto, date, cargo, start, end, date_work, cost);
                            }
                        }
                        connection.Close();
                    }
                    if (dialogResult == DialogResult.No)
                    {
                        MessageBox.Show("Заказ отменен");
                    }
                }
                //-----------------------------------------------------------------------------
            }
            else
            {
                MessageBox.Show("Введите все данные");
            }
        }

        private void makeEntry_Load(object sender, EventArgs e)//отрисовка кнопок в зависимости от уровня доступа вход в программу
        {
            try
            {
                connection.Open();
                show.update_database();
                connection.Close();
            }
            catch(Exception error)
            {
                this.Close();
            }
            login log = new login(connection, ref access);
            DialogResult dialogResult = log.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                this.Close();
            }
            else
            {
                access = log.Access;
                ability = log.Ability;
            }

            if (access != null)
            {
                int[] allowed_forms = ParseAccess(access);
                menu = new Button[menu_size];
                int _x = 0;
                int _y = 0;
                for (int i = 0; i < menu.Length; i++)
                {
                    if (checkShowElement(i, allowed_forms))
                    {
                        menu[i] = new Button();
                        menu[i].Text = menu_name[i];
                        menu[i].Name = "menu_button" + i;
                        menu[i].Size = new System.Drawing.Size(_width, _height);
                        menu[i].Location = new System.Drawing.Point(_x, _y);
                        menu[i].Click += new EventHandler(menu_button_Click);
                        this.Controls.Add(menu[i]);
                        _y += _height;
                    }
                }
            }
        }
        private int[] ParseAccess (string access)//разбор строки доступа
        {
            string[] accesses = access.Trim(new char[] { '(', ')' }).Split(',');
            int[] mas = new int[accesses.Length];
            for (int i = 0; i < accesses.Length; i++)
            {
                mas[i] = Convert.ToInt32(accesses[i]);
            }
            return mas;
        }
        private bool checkShowElement(int search, int[] allowed ) //проверка на доступность формы
        {
            for(int i = 0; i < allowed.Length; i++)
            {
                if (search == allowed[i]) return true;
            }
            return false;
        }


    }
}
