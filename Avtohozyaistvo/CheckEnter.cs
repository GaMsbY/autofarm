﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Drawing;

namespace Avtohozyaistvo
{
    class CheckEnter
    {
        //Класс проверки правильности ввода
        public bool checkFormatRichTextBox(string pattern, string value, RichTextBox comboBox, GroupBox form)
        {
            Pen p;
            Graphics g;
            int variance = 1;
            if (value == string.Empty)
            {
                comboBox.Text = string.Empty;
                p = new Pen(Color.Red);
                g = form.CreateGraphics();
                g.DrawRectangle(p, new Rectangle(comboBox.Location.X - variance, comboBox.Location.Y - variance, comboBox.Width + variance, comboBox.Height + variance));
                return false;
            }
            if (pattern != null)
            {
                if (!(Regex.IsMatch(value, pattern)))
                {
                    comboBox.Text = string.Empty;
                    p = new Pen(Color.Red);
                    g = form.CreateGraphics();
                    g.DrawRectangle(p, new Rectangle(comboBox.Location.X - variance, comboBox.Location.Y - variance, comboBox.Width + variance, comboBox.Height + variance));
                    return false;
                }
            }
            p = new Pen(Color.Blue);
            g = form.CreateGraphics();
            g.DrawRectangle(p, new Rectangle(comboBox.Location.X - variance, comboBox.Location.Y - variance, comboBox.Width + variance, comboBox.Height + variance));
            return true;
        }
        public bool checkFormatTextBox(string pattern, string value, TextBox comboBox, GroupBox form)
        {
            Pen p;
            Graphics g;
            int variance = 1;
            if (value == string.Empty)
            {
                comboBox.Text = string.Empty;
                p = new Pen(Color.Red);
                g = form.CreateGraphics();
                g.DrawRectangle(p, new Rectangle(comboBox.Location.X - variance, comboBox.Location.Y - variance, comboBox.Width + variance, comboBox.Height + variance));
                return false;
            }
            if (pattern != null)
            {
                if (!(Regex.IsMatch(value, pattern)))
                {
                    comboBox.Text = string.Empty;
                    p = new Pen(Color.Red);
                    g = form.CreateGraphics();
                    g.DrawRectangle(p, new Rectangle(comboBox.Location.X - variance, comboBox.Location.Y - variance, comboBox.Width + variance, comboBox.Height + variance));
                    return false;
                }
            }
            p = new Pen(Color.Blue);
            g = form.CreateGraphics();
            g.DrawRectangle(p, new Rectangle(comboBox.Location.X - variance, comboBox.Location.Y - variance, comboBox.Width + variance, comboBox.Height + variance));
            return true;
        }
        public bool checkFormatComboBox(string pattern, string value, ComboBox comboBox, GroupBox form)
        {
            Pen p;
            Graphics g;
            int variance = 1;
            if (value == string.Empty)
            {
                comboBox.Text = string.Empty;
                p = new Pen(Color.Red);
                g = form.CreateGraphics();
                g.DrawRectangle(p, new Rectangle(comboBox.Location.X - variance, comboBox.Location.Y - variance, comboBox.Width + variance, comboBox.Height + variance));
                return false;
            }
            if (pattern != null)
            {
                comboBox.Text = string.Empty;
                if (!(Regex.IsMatch(value, pattern)))
                {
                    p = new Pen(Color.Red);
                    g = form.CreateGraphics();
                    g.DrawRectangle(p, new Rectangle(comboBox.Location.X - variance, comboBox.Location.Y - variance, comboBox.Width + variance, comboBox.Height + variance));
                    return false;
                }
            }
            p = new Pen(Color.Blue);
            g = form.CreateGraphics();
            g.DrawRectangle(p, new Rectangle(comboBox.Location.X - variance, comboBox.Location.Y - variance, comboBox.Width + variance, comboBox.Height + variance));
            return true;
        }
    }
}
