﻿namespace Avtohozyaistvo
{
    partial class makeEntry
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.surname_tb = new System.Windows.Forms.TextBox();
            this.makeentry_gb = new System.Windows.Forms.GroupBox();
            this.cargo_tb = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.time_tb = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.date_tb = new System.Windows.Forms.DateTimePicker();
            this.weight_tb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.end_address_tb = new System.Windows.Forms.TextBox();
            this.start_address_tb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.telephon_tb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.address_tb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.make_entry_btn = new System.Windows.Forms.Button();
            this.makeentry_gb.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(90, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Фамилия";
            // 
            // surname_tb
            // 
            this.surname_tb.Location = new System.Drawing.Point(152, 12);
            this.surname_tb.Name = "surname_tb";
            this.surname_tb.Size = new System.Drawing.Size(126, 20);
            this.surname_tb.TabIndex = 0;
            // 
            // makeentry_gb
            // 
            this.makeentry_gb.Controls.Add(this.cargo_tb);
            this.makeentry_gb.Controls.Add(this.label9);
            this.makeentry_gb.Controls.Add(this.label8);
            this.makeentry_gb.Controls.Add(this.time_tb);
            this.makeentry_gb.Controls.Add(this.label7);
            this.makeentry_gb.Controls.Add(this.date_tb);
            this.makeentry_gb.Controls.Add(this.weight_tb);
            this.makeentry_gb.Controls.Add(this.label5);
            this.makeentry_gb.Controls.Add(this.label6);
            this.makeentry_gb.Controls.Add(this.end_address_tb);
            this.makeentry_gb.Controls.Add(this.start_address_tb);
            this.makeentry_gb.Controls.Add(this.label4);
            this.makeentry_gb.Controls.Add(this.telephon_tb);
            this.makeentry_gb.Controls.Add(this.label3);
            this.makeentry_gb.Controls.Add(this.address_tb);
            this.makeentry_gb.Controls.Add(this.label2);
            this.makeentry_gb.Controls.Add(this.surname_tb);
            this.makeentry_gb.Controls.Add(this.label1);
            this.makeentry_gb.Location = new System.Drawing.Point(105, 2);
            this.makeentry_gb.Name = "makeentry_gb";
            this.makeentry_gb.Size = new System.Drawing.Size(284, 248);
            this.makeentry_gb.TabIndex = 2;
            this.makeentry_gb.TabStop = false;
            this.makeentry_gb.Text = "Клиент";
            // 
            // cargo_tb
            // 
            this.cargo_tb.FormattingEnabled = true;
            this.cargo_tb.Location = new System.Drawing.Point(152, 167);
            this.cargo_tb.Name = "cargo_tb";
            this.cargo_tb.Size = new System.Drawing.Size(126, 21);
            this.cargo_tb.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(89, 170);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Тип груза";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(41, 222);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Время выполнения";
            // 
            // time_tb
            // 
            this.time_tb.Location = new System.Drawing.Point(152, 220);
            this.time_tb.Name = "time_tb";
            this.time_tb.Size = new System.Drawing.Size(126, 20);
            this.time_tb.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(48, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Дата выполнения";
            // 
            // date_tb
            // 
            this.date_tb.Location = new System.Drawing.Point(152, 194);
            this.date_tb.Name = "date_tb";
            this.date_tb.Size = new System.Drawing.Size(126, 20);
            this.date_tb.TabIndex = 12;
            // 
            // weight_tb
            // 
            this.weight_tb.Location = new System.Drawing.Point(152, 142);
            this.weight_tb.Name = "weight_tb";
            this.weight_tb.Size = new System.Drawing.Size(126, 20);
            this.weight_tb.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Адрес назначения";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(89, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Вес груза";
            // 
            // end_address_tb
            // 
            this.end_address_tb.Location = new System.Drawing.Point(152, 116);
            this.end_address_tb.Name = "end_address_tb";
            this.end_address_tb.Size = new System.Drawing.Size(126, 20);
            this.end_address_tb.TabIndex = 8;
            // 
            // start_address_tb
            // 
            this.start_address_tb.Location = new System.Drawing.Point(152, 90);
            this.start_address_tb.Name = "start_address_tb";
            this.start_address_tb.Size = new System.Drawing.Size(126, 20);
            this.start_address_tb.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(70, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Адрес подачи";
            // 
            // telephon_tb
            // 
            this.telephon_tb.Location = new System.Drawing.Point(152, 64);
            this.telephon_tb.Name = "telephon_tb";
            this.telephon_tb.Size = new System.Drawing.Size(126, 20);
            this.telephon_tb.TabIndex = 4;
            this.telephon_tb.Tag = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(94, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Телефон";
            // 
            // address_tb
            // 
            this.address_tb.Location = new System.Drawing.Point(152, 38);
            this.address_tb.Name = "address_tb";
            this.address_tb.Size = new System.Drawing.Size(126, 20);
            this.address_tb.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(108, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Адрес";
            // 
            // make_entry_btn
            // 
            this.make_entry_btn.Location = new System.Drawing.Point(105, 256);
            this.make_entry_btn.Name = "make_entry_btn";
            this.make_entry_btn.Size = new System.Drawing.Size(284, 23);
            this.make_entry_btn.TabIndex = 3;
            this.make_entry_btn.Text = "Оформить заказ";
            this.make_entry_btn.UseVisualStyleBackColor = true;
            this.make_entry_btn.Click += new System.EventHandler(this.make_entry_btn_Click);
            // 
            // makeEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 286);
            this.Controls.Add(this.make_entry_btn);
            this.Controls.Add(this.makeentry_gb);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "makeEntry";
            this.Text = "Автохозяйство";
            this.Load += new System.EventHandler(this.makeEntry_Load);
            this.makeentry_gb.ResumeLayout(false);
            this.makeentry_gb.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox surname_tb;
        private System.Windows.Forms.GroupBox makeentry_gb;
        private System.Windows.Forms.TextBox telephon_tb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox address_tb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker time_tb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker date_tb;
        private System.Windows.Forms.TextBox weight_tb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox end_address_tb;
        private System.Windows.Forms.TextBox start_address_tb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button make_entry_btn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cargo_tb;
    }
}

