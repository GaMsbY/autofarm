﻿namespace Avtohozyaistvo
{
    partial class entrys
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.client_tb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.date_order_tb = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.delete_btn = new System.Windows.Forms.Button();
            this.add_btn = new System.Windows.Forms.Button();
            this.table_GV = new System.Windows.Forms.DataGridView();
            this.auto_tb = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.driver_tb = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cargo_tb = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.address_start_tb = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.address_end_tb = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.date_work_tb = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.weight_tb = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.state_tb = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.time_tb = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.entrys_gb = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.table_GV)).BeginInit();
            this.entrys_gb.SuspendLayout();
            this.SuspendLayout();
            // 
            // client_tb
            // 
            this.client_tb.FormattingEnabled = true;
            this.client_tb.Location = new System.Drawing.Point(117, 19);
            this.client_tb.Name = "client_tb";
            this.client_tb.Size = new System.Drawing.Size(390, 21);
            this.client_tb.TabIndex = 41;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(68, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 40;
            this.label6.Text = "Клиент";
            // 
            // date_order_tb
            // 
            this.date_order_tb.Location = new System.Drawing.Point(118, 100);
            this.date_order_tb.Name = "date_order_tb";
            this.date_order_tb.Size = new System.Drawing.Size(389, 20);
            this.date_order_tb.TabIndex = 37;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 36;
            this.label4.Text = "Дата заказа";
            // 
            // delete_btn
            // 
            this.delete_btn.Location = new System.Drawing.Point(1155, 447);
            this.delete_btn.Name = "delete_btn";
            this.delete_btn.Size = new System.Drawing.Size(97, 23);
            this.delete_btn.TabIndex = 29;
            this.delete_btn.Text = "Удалить";
            this.delete_btn.UseVisualStyleBackColor = true;
            this.delete_btn.Click += new System.EventHandler(this.delete_btn_Click);
            // 
            // add_btn
            // 
            this.add_btn.Location = new System.Drawing.Point(12, 447);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(97, 23);
            this.add_btn.TabIndex = 28;
            this.add_btn.Text = "Добавить в БД";
            this.add_btn.UseVisualStyleBackColor = true;
            this.add_btn.Click += new System.EventHandler(this.add_btn_Click);
            // 
            // table_GV
            // 
            this.table_GV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.table_GV.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.table_GV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_GV.Location = new System.Drawing.Point(12, 12);
            this.table_GV.Name = "table_GV";
            this.table_GV.Size = new System.Drawing.Size(1240, 111);
            this.table_GV.TabIndex = 27;
            // 
            // auto_tb
            // 
            this.auto_tb.FormattingEnabled = true;
            this.auto_tb.Location = new System.Drawing.Point(117, 46);
            this.auto_tb.Name = "auto_tb";
            this.auto_tb.Size = new System.Drawing.Size(390, 21);
            this.auto_tb.TabIndex = 43;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 42;
            this.label1.Text = "Автомобиль";
            // 
            // driver_tb
            // 
            this.driver_tb.FormattingEnabled = true;
            this.driver_tb.Location = new System.Drawing.Point(117, 73);
            this.driver_tb.Name = "driver_tb";
            this.driver_tb.Size = new System.Drawing.Size(390, 21);
            this.driver_tb.TabIndex = 45;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(56, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 44;
            this.label7.Text = "Водитель";
            // 
            // cargo_tb
            // 
            this.cargo_tb.FormattingEnabled = true;
            this.cargo_tb.Location = new System.Drawing.Point(117, 126);
            this.cargo_tb.Name = "cargo_tb";
            this.cargo_tb.Size = new System.Drawing.Size(390, 21);
            this.cargo_tb.TabIndex = 47;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(81, 129);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 46;
            this.label8.Text = "Груз";
            // 
            // address_start_tb
            // 
            this.address_start_tb.Location = new System.Drawing.Point(118, 153);
            this.address_start_tb.Name = "address_start_tb";
            this.address_start_tb.Size = new System.Drawing.Size(389, 20);
            this.address_start_tb.TabIndex = 49;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(36, 156);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 13);
            this.label9.TabIndex = 48;
            this.label9.Text = "Адрес подачи";
            // 
            // address_end_tb
            // 
            this.address_end_tb.Location = new System.Drawing.Point(118, 179);
            this.address_end_tb.Name = "address_end_tb";
            this.address_end_tb.Size = new System.Drawing.Size(389, 20);
            this.address_end_tb.TabIndex = 51;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 182);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 13);
            this.label10.TabIndex = 50;
            this.label10.Text = "Адрес назначения";
            // 
            // date_work_tb
            // 
            this.date_work_tb.Location = new System.Drawing.Point(118, 205);
            this.date_work_tb.Name = "date_work_tb";
            this.date_work_tb.Size = new System.Drawing.Size(389, 20);
            this.date_work_tb.TabIndex = 53;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 207);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 13);
            this.label11.TabIndex = 52;
            this.label11.Text = "Дата выполнения";
            // 
            // weight_tb
            // 
            this.weight_tb.Location = new System.Drawing.Point(118, 257);
            this.weight_tb.Name = "weight_tb";
            this.weight_tb.Size = new System.Drawing.Size(389, 20);
            this.weight_tb.TabIndex = 55;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(85, 260);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(26, 13);
            this.label12.TabIndex = 54;
            this.label12.Text = "Вес";
            // 
            // state_tb
            // 
            this.state_tb.Location = new System.Drawing.Point(118, 283);
            this.state_tb.Name = "state_tb";
            this.state_tb.Size = new System.Drawing.Size(389, 20);
            this.state_tb.TabIndex = 57;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(71, 286);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 56;
            this.label13.Text = "Статус";
            // 
            // time_tb
            // 
            this.time_tb.Location = new System.Drawing.Point(118, 231);
            this.time_tb.Name = "time_tb";
            this.time_tb.Size = new System.Drawing.Size(389, 20);
            this.time_tb.TabIndex = 59;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 58;
            this.label2.Text = "Время выполнения";
            // 
            // entrys_gb
            // 
            this.entrys_gb.Controls.Add(this.client_tb);
            this.entrys_gb.Controls.Add(this.time_tb);
            this.entrys_gb.Controls.Add(this.label4);
            this.entrys_gb.Controls.Add(this.label2);
            this.entrys_gb.Controls.Add(this.date_order_tb);
            this.entrys_gb.Controls.Add(this.state_tb);
            this.entrys_gb.Controls.Add(this.label6);
            this.entrys_gb.Controls.Add(this.label13);
            this.entrys_gb.Controls.Add(this.label1);
            this.entrys_gb.Controls.Add(this.weight_tb);
            this.entrys_gb.Controls.Add(this.auto_tb);
            this.entrys_gb.Controls.Add(this.label12);
            this.entrys_gb.Controls.Add(this.label7);
            this.entrys_gb.Controls.Add(this.date_work_tb);
            this.entrys_gb.Controls.Add(this.driver_tb);
            this.entrys_gb.Controls.Add(this.label11);
            this.entrys_gb.Controls.Add(this.label8);
            this.entrys_gb.Controls.Add(this.address_end_tb);
            this.entrys_gb.Controls.Add(this.cargo_tb);
            this.entrys_gb.Controls.Add(this.label10);
            this.entrys_gb.Controls.Add(this.label9);
            this.entrys_gb.Controls.Add(this.address_start_tb);
            this.entrys_gb.Location = new System.Drawing.Point(344, 129);
            this.entrys_gb.Name = "entrys_gb";
            this.entrys_gb.Size = new System.Drawing.Size(513, 312);
            this.entrys_gb.TabIndex = 60;
            this.entrys_gb.TabStop = false;
            this.entrys_gb.Text = "Заказ";
            // 
            // entrys
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 476);
            this.Controls.Add(this.entrys_gb);
            this.Controls.Add(this.delete_btn);
            this.Controls.Add(this.add_btn);
            this.Controls.Add(this.table_GV);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "entrys";
            this.Text = "Заказы";
            this.Load += new System.EventHandler(this.entrys_Load);
            ((System.ComponentModel.ISupportInitialize)(this.table_GV)).EndInit();
            this.entrys_gb.ResumeLayout(false);
            this.entrys_gb.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox client_tb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker date_order_tb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button delete_btn;
        private System.Windows.Forms.Button add_btn;
        private System.Windows.Forms.DataGridView table_GV;
        private System.Windows.Forms.ComboBox auto_tb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox driver_tb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cargo_tb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox address_start_tb;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox address_end_tb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker date_work_tb;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox weight_tb;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox state_tb;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker time_tb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox entrys_gb;
    }
}