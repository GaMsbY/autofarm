﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Avtohozyaistvo
{
    public partial class entrys : Form
    {
        MySqlConnection connection;
        ShowTables show;
        CheckEnter checkEnter = new CheckEnter();
        bool client_ch = false;
        bool auto_ch = false;
        bool driver_ch = false;
        bool cargo_ch = false;
        bool start_address_ch = false;
        bool end_address_ch = false;
        bool weight_ch = false;
        bool state_ch = false;
        string ability = null;
        string show_request = "SELECT cl.surname AS Фамилия, cl.address AS Адрес, cl.telephon AS Телефон, " +
            "au.mark AS Марка, au.number_g AS ГосНомер, dr.surname AS Фамилия, " +
            "dr.name AS Имя, dr.patronymic AS Отчество, en.date_order AS Дата_заказа, " +
            "car.cargo AS Груз, en.start_address AS Адрес_отправки, en.end_address AS Адрес_назначения, " +
            "en.date_work AS Дата_время_работы, en.weight AS Вес_ц, en.state_e AS Состяние_заказа " +
            "FROM entrys en " +
            "INNER JOIN clients cl ON cl.id_client=en.id_client " +
            "INNER JOIN autos au ON au.id_auto=en.id_auto " +
            "INNER JOIN drivers dr ON dr.id_driver=en.id_driver " +
            "INNER JOIN cargos car ON car.id_cargo=en.id_cargo";
        public entrys(MySqlConnection connection, string ability)
        {
            string request = null;

            InitializeComponent();
            this.connection = connection;
            this.ability = ability;
            table_GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            table_GV.AllowUserToAddRows = false;
            table_GV.MultiSelect = false;
            table_GV.ReadOnly = true;
            time_tb.Format = DateTimePickerFormat.Time;
            time_tb.ShowUpDown = true;

            if (ability == "read")
            {
                add_btn.Visible = false;
                delete_btn.Visible = false;
                entrys_gb.Visible = false;
                table_GV.Height = this.Height - 60;
            }

            connection.Open();
            show = new ShowTables(connection, table_GV, "entrys");
            show.take_data(show_request);

            request = "SELECT surname, address, telephon FROM clients";
            show.get_selector(request, client_tb);
            connection.Close();

            request = "SELECT mark, number_g FROM autos";
            connection.Open();
            show.get_selector(request, auto_tb);
            connection.Close();

            request = "SELECT surname, name, patronymic FROM drivers";
            connection.Open();
            show.get_selector(request, driver_tb);
            connection.Close();

            request = "SELECT cargo FROM cargos";
            connection.Open();
            show.get_selector(request, cargo_tb);
            connection.Close();

            client_tb.TextChanged += Client_tb_TextChanged;
            auto_tb.TextChanged += Auto_tb_TextChanged;
            driver_tb.TextChanged += Driver_tb_TextChanged;
            cargo_tb.TextChanged += Cargo_tb_TextChanged;
            address_start_tb.TextChanged += Address_start_tb_TextChanged;
            address_end_tb.TextChanged += Address_end_tb_TextChanged;
            weight_tb.TextChanged += Weight_tb_TextChanged;
            state_tb.TextChanged += State_tb_TextChanged;
        }

        private void State_tb_TextChanged(object sender, EventArgs e)
        {
            state_ch = checkEnter.checkFormatTextBox(@"^[а-яА-Я ]+$", state_tb.Text, state_tb, this.entrys_gb);
        }

        private void Weight_tb_TextChanged(object sender, EventArgs e)
        {
            weight_ch = checkEnter.checkFormatTextBox(@"^\d+$", weight_tb.Text, weight_tb, this.entrys_gb);
        }

        private void Address_end_tb_TextChanged(object sender, EventArgs e)
        {
            end_address_ch = checkEnter.checkFormatTextBox(@"^[а-яА-Я\d ,.]+$", address_end_tb.Text, address_end_tb, this.entrys_gb);
        }

        private void Address_start_tb_TextChanged(object sender, EventArgs e)
        {
            start_address_ch = checkEnter.checkFormatTextBox(@"^[а-яА-Я\d ,.]+$", address_start_tb.Text, address_start_tb, this.entrys_gb);
        }

        private void Cargo_tb_TextChanged(object sender, EventArgs e)
        {
            cargo_ch = checkEnter.checkFormatComboBox(null, cargo_tb.Text, cargo_tb, this.entrys_gb);
        }

        private void Driver_tb_TextChanged(object sender, EventArgs e)
        {
            driver_ch = checkEnter.checkFormatComboBox(null, driver_tb.Text, driver_tb, this.entrys_gb);
        }

        private void Auto_tb_TextChanged(object sender, EventArgs e)
        {
            auto_ch = checkEnter.checkFormatComboBox(null, auto_tb.Text, auto_tb, this.entrys_gb);
        }

        private void Client_tb_TextChanged(object sender, EventArgs e)
        {
            client_ch = checkEnter.checkFormatComboBox(null, client_tb.Text, client_tb, this.entrys_gb);
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            if (client_ch && auto_ch && driver_ch && cargo_ch && start_address_ch && end_address_ch && weight_ch && state_ch)
            {
                string request = null;

                string[] client = client_tb.Text.Split(' ');
                string[] auto = auto_tb.Text.Split(' ');
                string[] driver = driver_tb.Text.Split(' ');
                DateTime date_order = date_order_tb.Value;
                string cargo = cargo_tb.Text;
                string start_address = address_start_tb.Text;
                string end_address = address_end_tb.Text;
                DateTime date_time_work = date_work_tb.Value.Date + time_tb.Value.TimeOfDay;
                double weight;
                double.TryParse(weight_tb.Text, out weight);
                string state = state_tb.Text;

                request = "SELECT id_client FROM clients " +
                    "WHERE surname =\"" + client[0] + "\" " +
                    "AND telephon=\"" + client[client.Length - 1] + "\"";
                connection.Open();
                int id_client = show.get_id(request);
                connection.Close();

                request = "SELECT id_auto FROM autos " +
                    "WHERE mark =\"" + auto[0] + "\" " +
                    "AND number_g=\"" + auto[1] + "\"";
                connection.Open();
                int id_auto = show.get_id(request);
                connection.Close();

                request = "SELECT id_driver FROM drivers " +
                    "WHERE surname =\"" + driver[0] + "\" " +
                    "AND name=\"" + driver[1] + "\" " +
                    "AND patronymic=\"" + driver[2] + "\"";
                connection.Open();
                int id_driver = show.get_id(request);
                connection.Close();

                request = "SELECT id_cargo FROM cargos WHERE cargo =\"" + cargo + "\"";
                connection.Open();
                int id_cargo = show.get_id(request);
                connection.Close();

                request = "SELECT unit_cost FROM cargos WHERE cargo =\"" + cargo + "\"";
                connection.Open();
                int unit_cost = show.get_id(request);
                connection.Close();

                request = "INSERT INTO entrys(id_client, id_auto, id_driver, date_order, id_cargo, " +
                    "start_address, end_address, date_work, weight, state_e, cost_en) " +
                    "VALUES(" + id_client + "," + id_auto + "," + id_driver + ",\"" + date_order.Date.ToString("yyyy-MM-dd") + "\"," +
                    "" + id_cargo + ",\"" + start_address + "\",\"" + end_address + "\"," +
                    "\"" + date_time_work.ToString("yyyy-MM-dd HH:mm:ss") + "\"," +
                    "" + weight + ",\"" + state + "\"," + weight * unit_cost + ");";
                connection.Open();
                show.make_request(request);
                connection.Close();
            }
            else
            {
                MessageBox.Show("Введите все данные");
            }
        }

        private void delete_btn_Click(object sender, EventArgs e)
        {
            string request = null;

            DataGridViewRow row = table_GV.SelectedRows[0];
            string surname = (string)row.Cells[0].Value;
            string address = (string)row.Cells[1].Value;
            string telephon = (string)row.Cells[2].Value;
            DateTime date_order = (DateTime)row.Cells[8].Value;

            request = "SELECT id_client FROM clients " +
                "WHERE surname =\"" + surname + "\" " +
                "AND address=\"" + address + "\" " +
                "AND telephon=\"" + telephon + "\"";
            connection.Open();
            int id_client = show.get_id(request);
            connection.Close();

            request = "DELETE FROM entrys " +
                "WHERE id_client=\"" + id_client + "\" " +
                "AND date_order=\"" + date_order.Date.ToString("yyyy-MM-dd") + "\";";
            connection.Open();
            show.make_request(request);
            connection.Close();
        }

        private void entrys_Load(object sender, EventArgs e)
        {
            connection.Open();
            show.update_database();
            connection.Close();
        }
    }
}
