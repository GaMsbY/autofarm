﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Avtohozyaistvo
{
    public partial class displacement : Form
    {
        MySqlConnection connection;
        ShowTables show;
        CheckEnter checkEnter = new CheckEnter();
        bool employee_ch = false;
        bool position_ch = false;
        bool couse_ch = false;
        string ability = null;
        string show_request = "SELECT em.surname AS Фамилия, em.name AS Имя, em.patronymic AS Отчество, " +
            "pos.position AS Должность, dis.date_order AS Дата_приказа, dis.couse_displacement AS Причина " +
            "FROM information_displacement dis INNER JOIN positions pos ON pos.id_position = dis.id_position " +
            "INNER JOIN employees em ON em.id_associate=dis.id_associate;";
        public displacement(MySqlConnection connection, string ability)
        {
            string request = null;

            InitializeComponent();
            this.connection = connection;
            this.ability = ability;
            table_GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            table_GV.AllowUserToAddRows = false;
            table_GV.MultiSelect = false;
            table_GV.ReadOnly = true;

            if (ability == "read")
            {
                add_btn.Visible = false;
                delete_btn.Visible = false;
                displacement_gb.Visible = false;
                table_GV.Height = this.Height - 60;
            }

            connection.Open();
            show = new ShowTables(connection, table_GV, "displacement");
            show.take_data(show_request);

            request = "SELECT position FROM positions";
            show.get_selector(request, position_tb);
            connection.Close();

            request = "SELECT surname, name, patronymic FROM employees";
            connection.Open();
            show.get_selector(request, associate_tb);
            connection.Close();

            associate_tb.TextChanged += Associate_tb_TextChanged;
            position_tb.TextChanged += Position_tb_TextChanged;
            couse_tb.TextChanged += Couse_tb_TextChanged;
        }

        private void Couse_tb_TextChanged(object sender, EventArgs e)
        {
            couse_ch = checkEnter.checkFormatRichTextBox(null, couse_tb.Text, couse_tb, this.displacement_gb);
        }

        private void Position_tb_TextChanged(object sender, EventArgs e)
        {
            position_ch = checkEnter.checkFormatComboBox(null, position_tb.Text, position_tb, this.displacement_gb);
        }

        private void Associate_tb_TextChanged(object sender, EventArgs e)
        {
            employee_ch = checkEnter.checkFormatComboBox(null, associate_tb.Text, associate_tb, this.displacement_gb);
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            if (employee_ch && position_ch && couse_ch)
            {
                string request = null;

                string associate = associate_tb.Text;
                string position = position_tb.Text;
                DateTime date_order = date_tb.Value;
                string couse = couse_tb.Text;
                string[] name_surname = associate.Split(' ');

                int id_position = 0;
                int id_associate = 0;

                request = "SELECT id_position FROM positions WHERE position =\"" + position + "\"";
                connection.Open();
                id_position = show.get_id(request);
                connection.Close();

                request = "SELECT id_associate FROM employees " +
                    "WHERE surname =\"" + name_surname[0] + "\" " +
                    "AND name=\"" + name_surname[1] + "\" " +
                    "AND patronymic=\"" + name_surname[2] + "\"";
                connection.Open();
                id_associate = show.get_id(request);
                connection.Close();

                request = "INSERT INTO information_displacement(id_associate, id_position, date_order, couse_displacement) " +
                    "VALUES(" + id_associate + "," + id_position + ",\"" + date_order.Date.ToString("yyyy-MM-dd") + "\",\"" + couse + "\");";
                connection.Open();
                show.make_request(request);
                connection.Close();

                request = "UPDATE employees " +
                    "SET id_position=\"" + id_position + "\" " +
                    "WHERE id_associate = \"" + id_associate + "\"";
                connection.Open();
                show.make_request(request);
                connection.Close();
            }
            else
            {
                MessageBox.Show("Введите все данные");
            }
        }

        private void delete_btn_Click(object sender, EventArgs e)
        {
            string request = null;

            DataGridViewRow row = table_GV.SelectedRows[0];
            string associate = (string)row.Cells[1].Value + " " + (string)row.Cells[2].Value + " " + (string)row.Cells[3].Value;
            string position = (string)row.Cells[4].Value;
            DateTime date_order = (DateTime)row.Cells[5].Value;
            string couse = (string)row.Cells[6].Value;
            string[] associates = associate.Split(' ');

            int id_position = 0;
            int id_associate = 0;

            request = "SELECT id_position FROM positions WHERE position =\"" + position + "\"";
            connection.Open();
            id_position = show.get_id(request);
            connection.Close();

            request = "SELECT id_associate " +
                "FROM employees " +
                "WHERE surname =\"" + associates[0] + "\" " +
                "AND name=\"" + associates[1] + "\" " +
                "AND patronymic=\"" + associates[2] + "\"";
            connection.Open();
            id_associate = show.get_id(request);
            connection.Close();

            request = "DELETE FROM information_displacement " +
                "WHERE id_associate=" + id_associate + " " +
                "AND id_position =" + id_position + " " +
                "AND date_order=\"" + date_order.Date.ToString("yyyy-MM-dd") + "\";";
            connection.Open();
            show.make_request(request);
            connection.Close();
        }

        private void displacement_Load(object sender, EventArgs e)
        {
            connection.Open();
            show.update_database();
            connection.Close();
        }
    }
}
