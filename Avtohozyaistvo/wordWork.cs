﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Word = Microsoft.Office.Interop.Word;
using System.Reflection;
using System.Windows.Forms;
using System.Drawing;



namespace Avtohozyaistvo
{
    class wordWork
    {
        private readonly int id_entry;
        private  string surname;
        private  string address;
        private  string telephon;
        private  string execotor;
        private  string executor_auto;
        private  string date;
        private  string cargo;
        private  string start;
        private  string end;
        private  string date_work;
        private  string cost;

        public wordWork(int id_entry, string surname, string address, string telephon, string execotor, string executor_auto, string date, string cargo, string start, string end, string date_work, string cost)
        {
            this.id_entry = id_entry;
            this.surname = surname;
            this.address = address;
            this.telephon = telephon;
            this.execotor = execotor;
            this.executor_auto = executor_auto;
            this.date = date;
            this.cargo = cargo;
            this.start = start;
            this.end = end;
            this.date_work = date_work;
            this.cost = cost;
            CreateDoc();
        }
        public void CreateDoc()
        {
            try
            {
                Word.Application winword = new Word.Application();
                winword.Visible = false;
                //Заголовок документа
                winword.Documents.Application.Caption = "Квитанция о приеме заказа";
                object missing = Missing.Value;
                //Создание нового документа
                Word.Document document = winword.Documents.Add(ref missing, ref missing, ref missing, ref missing);

                //Добавление текста со стилем Заголовок 1
                Word.Paragraph para1 = document.Content.Paragraphs.Add(ref missing);
                para1.Range.Font.ColorIndex = Word.WdColorIndex.wdBlack;
                para1.Range.Font.Size = 16;
                para1.Range.Text = "Квитанция о приеме заказа";
                para1.Range.InsertParagraphAfter();

                Word.Paragraph para2 = document.Content.Paragraphs.Add(ref missing);
                para2.Range.Font.ColorIndex = Word.WdColorIndex.wdBlack;
                para2.Range.Font.Size = 12;
                para2.Range.Text += surname;
                para2.Range.Text += address;
                para2.Range.Text += telephon;
                para2.Range.Text += execotor;
                para2.Range.Text += executor_auto;
                para2.Range.Text += cargo;
                para2.Range.Text += start;
                para2.Range.Text += end;
                para2.Range.Text += date_work;
                para2.Range.Text += cost;
                para2.Range.Text += date;
                para2.Range.InsertParagraphAfter();

                //winword.Visible = true;
                //Сохранение документа
                DateTime now = DateTime.Now;
                string filename = "Квитанция" + id_entry.ToString();
                object filepath = @"C:\Users\slava\OneDrive\Рабочий стол\БД Курсач\Курсач\Avtohozyaistvo\" + filename;
                document.SaveAs(ref filepath);
                //Закрытие текущего документа
                document.Close(ref missing, ref missing, ref missing);
                document = null;
                //Закрытие приложения Word
                winword.Quit(ref missing, ref missing, ref missing);
                winword = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
