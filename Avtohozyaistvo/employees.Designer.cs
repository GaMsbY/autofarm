﻿namespace Avtohozyaistvo
{
    partial class employees
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.delete_btn = new System.Windows.Forms.Button();
            this.add_btn = new System.Windows.Forms.Button();
            this.table_GV = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.surname_tb = new System.Windows.Forms.TextBox();
            this.name_tb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.patronymic_tb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.date_tb = new System.Windows.Forms.DateTimePicker();
            this.address_tb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.position_tb = new System.Windows.Forms.ComboBox();
            this.employees_gb = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.table_GV)).BeginInit();
            this.employees_gb.SuspendLayout();
            this.SuspendLayout();
            // 
            // delete_btn
            // 
            this.delete_btn.Location = new System.Drawing.Point(695, 315);
            this.delete_btn.Name = "delete_btn";
            this.delete_btn.Size = new System.Drawing.Size(97, 23);
            this.delete_btn.TabIndex = 11;
            this.delete_btn.Text = "Удалить";
            this.delete_btn.UseVisualStyleBackColor = true;
            this.delete_btn.Click += new System.EventHandler(this.delete_btn_Click);
            // 
            // add_btn
            // 
            this.add_btn.Location = new System.Drawing.Point(12, 315);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(97, 23);
            this.add_btn.TabIndex = 10;
            this.add_btn.Text = "Добавить в БД";
            this.add_btn.UseVisualStyleBackColor = true;
            this.add_btn.Click += new System.EventHandler(this.add_btn_Click);
            // 
            // table_GV
            // 
            this.table_GV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.table_GV.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.table_GV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_GV.Location = new System.Drawing.Point(12, 12);
            this.table_GV.Name = "table_GV";
            this.table_GV.Size = new System.Drawing.Size(780, 111);
            this.table_GV.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Фамилия";
            // 
            // surname_tb
            // 
            this.surname_tb.Location = new System.Drawing.Point(98, 19);
            this.surname_tb.Name = "surname_tb";
            this.surname_tb.Size = new System.Drawing.Size(376, 20);
            this.surname_tb.TabIndex = 14;
            // 
            // name_tb
            // 
            this.name_tb.Location = new System.Drawing.Point(98, 45);
            this.name_tb.Name = "name_tb";
            this.name_tb.Size = new System.Drawing.Size(376, 20);
            this.name_tb.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Имя";
            // 
            // patronymic_tb
            // 
            this.patronymic_tb.Location = new System.Drawing.Point(98, 71);
            this.patronymic_tb.Name = "patronymic_tb";
            this.patronymic_tb.Size = new System.Drawing.Size(376, 20);
            this.patronymic_tb.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Отчество";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 123);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Дата рождения";
            // 
            // date_tb
            // 
            this.date_tb.Location = new System.Drawing.Point(98, 123);
            this.date_tb.Name = "date_tb";
            this.date_tb.Size = new System.Drawing.Size(376, 20);
            this.date_tb.TabIndex = 21;
            // 
            // address_tb
            // 
            this.address_tb.Location = new System.Drawing.Point(98, 97);
            this.address_tb.Name = "address_tb";
            this.address_tb.Size = new System.Drawing.Size(376, 20);
            this.address_tb.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Адрес";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Должность";
            // 
            // position_tb
            // 
            this.position_tb.FormattingEnabled = true;
            this.position_tb.Location = new System.Drawing.Point(97, 149);
            this.position_tb.Name = "position_tb";
            this.position_tb.Size = new System.Drawing.Size(377, 21);
            this.position_tb.TabIndex = 26;
            // 
            // employees_gb
            // 
            this.employees_gb.Controls.Add(this.surname_tb);
            this.employees_gb.Controls.Add(this.position_tb);
            this.employees_gb.Controls.Add(this.label1);
            this.employees_gb.Controls.Add(this.label6);
            this.employees_gb.Controls.Add(this.label2);
            this.employees_gb.Controls.Add(this.address_tb);
            this.employees_gb.Controls.Add(this.name_tb);
            this.employees_gb.Controls.Add(this.label5);
            this.employees_gb.Controls.Add(this.label3);
            this.employees_gb.Controls.Add(this.date_tb);
            this.employees_gb.Controls.Add(this.patronymic_tb);
            this.employees_gb.Controls.Add(this.label4);
            this.employees_gb.Location = new System.Drawing.Point(160, 129);
            this.employees_gb.Name = "employees_gb";
            this.employees_gb.Size = new System.Drawing.Size(480, 180);
            this.employees_gb.TabIndex = 27;
            this.employees_gb.TabStop = false;
            this.employees_gb.Text = "Сотрудник";
            // 
            // employees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 351);
            this.Controls.Add(this.employees_gb);
            this.Controls.Add(this.delete_btn);
            this.Controls.Add(this.add_btn);
            this.Controls.Add(this.table_GV);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "employees";
            this.Text = "Работники";
            this.Load += new System.EventHandler(this.employees_Load);
            ((System.ComponentModel.ISupportInitialize)(this.table_GV)).EndInit();
            this.employees_gb.ResumeLayout(false);
            this.employees_gb.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button delete_btn;
        private System.Windows.Forms.Button add_btn;
        private System.Windows.Forms.DataGridView table_GV;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox surname_tb;
        private System.Windows.Forms.TextBox name_tb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox patronymic_tb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker date_tb;
        private System.Windows.Forms.TextBox address_tb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox position_tb;
        private System.Windows.Forms.GroupBox employees_gb;
    }
}