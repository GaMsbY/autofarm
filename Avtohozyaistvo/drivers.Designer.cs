﻿namespace Avtohozyaistvo
{
    partial class drivers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.class_tb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.experience_tb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.delete_btn = new System.Windows.Forms.Button();
            this.add_btn = new System.Windows.Forms.Button();
            this.table_GV = new System.Windows.Forms.DataGridView();
            this.driver_tb = new System.Windows.Forms.ComboBox();
            this.driver_gb = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.table_GV)).BeginInit();
            this.driver_gb.SuspendLayout();
            this.SuspendLayout();
            // 
            // class_tb
            // 
            this.class_tb.FormattingEnabled = true;
            this.class_tb.Location = new System.Drawing.Point(81, 46);
            this.class_tb.Name = "class_tb";
            this.class_tb.Size = new System.Drawing.Size(392, 21);
            this.class_tb.TabIndex = 41;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 40;
            this.label6.Text = "Классность";
            // 
            // experience_tb
            // 
            this.experience_tb.Location = new System.Drawing.Point(82, 73);
            this.experience_tb.Name = "experience_tb";
            this.experience_tb.Size = new System.Drawing.Size(391, 20);
            this.experience_tb.TabIndex = 39;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 38;
            this.label5.Text = "Стаж";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Водитель";
            // 
            // delete_btn
            // 
            this.delete_btn.Location = new System.Drawing.Point(695, 237);
            this.delete_btn.Name = "delete_btn";
            this.delete_btn.Size = new System.Drawing.Size(97, 23);
            this.delete_btn.TabIndex = 29;
            this.delete_btn.Text = "Удалить";
            this.delete_btn.UseVisualStyleBackColor = true;
            this.delete_btn.Click += new System.EventHandler(this.delete_btn_Click);
            // 
            // add_btn
            // 
            this.add_btn.Location = new System.Drawing.Point(12, 240);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(97, 23);
            this.add_btn.TabIndex = 28;
            this.add_btn.Text = "Добавить в БД";
            this.add_btn.UseVisualStyleBackColor = true;
            this.add_btn.Click += new System.EventHandler(this.add_btn_Click);
            // 
            // table_GV
            // 
            this.table_GV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.table_GV.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.table_GV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_GV.Location = new System.Drawing.Point(12, 12);
            this.table_GV.Name = "table_GV";
            this.table_GV.Size = new System.Drawing.Size(780, 111);
            this.table_GV.TabIndex = 27;
            // 
            // driver_tb
            // 
            this.driver_tb.FormattingEnabled = true;
            this.driver_tb.Location = new System.Drawing.Point(81, 19);
            this.driver_tb.Name = "driver_tb";
            this.driver_tb.Size = new System.Drawing.Size(392, 21);
            this.driver_tb.TabIndex = 42;
            // 
            // driver_gb
            // 
            this.driver_gb.Controls.Add(this.driver_tb);
            this.driver_gb.Controls.Add(this.label1);
            this.driver_gb.Controls.Add(this.class_tb);
            this.driver_gb.Controls.Add(this.label5);
            this.driver_gb.Controls.Add(this.label6);
            this.driver_gb.Controls.Add(this.experience_tb);
            this.driver_gb.Location = new System.Drawing.Point(162, 129);
            this.driver_gb.Name = "driver_gb";
            this.driver_gb.Size = new System.Drawing.Size(479, 105);
            this.driver_gb.TabIndex = 43;
            this.driver_gb.TabStop = false;
            this.driver_gb.Text = "Водитель";
            // 
            // drivers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 272);
            this.Controls.Add(this.driver_gb);
            this.Controls.Add(this.delete_btn);
            this.Controls.Add(this.add_btn);
            this.Controls.Add(this.table_GV);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "drivers";
            this.Text = "Водители";
            this.Load += new System.EventHandler(this.drivers_Load);
            ((System.ComponentModel.ISupportInitialize)(this.table_GV)).EndInit();
            this.driver_gb.ResumeLayout(false);
            this.driver_gb.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox class_tb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox experience_tb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button delete_btn;
        private System.Windows.Forms.Button add_btn;
        private System.Windows.Forms.DataGridView table_GV;
        private System.Windows.Forms.ComboBox driver_tb;
        private System.Windows.Forms.GroupBox driver_gb;
    }
}